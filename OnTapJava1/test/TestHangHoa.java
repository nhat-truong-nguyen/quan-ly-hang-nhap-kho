
import java.util.ArrayList;
import ontapjava1.HangHoaModel;
import org.junit.Assert;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nhatt
 */
public class TestHangHoa {

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorHangHoaMaHang_Abnormal() {
        new HangHoaModel("  ", "Laptop", 20000000, 10, "10/8/22", "My");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorHangHoaTen_Abnormal() {
        new HangHoaModel("1", "   ", 20000000, 10, "10/8/22", "My");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorHangHoaGia_Abnormal() {
        new HangHoaModel("1", "Laptop", -20000000, 10, "10/8/22", "My");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorHangHoaSoLuong_Abnormal() {
        new HangHoaModel("1", "Laptop", 20000000, -10, "10/8/22", "My");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorHangHoaNgaySanXuat_Abnormal() {
        new HangHoaModel("1", "Laptop", 20000000, 10, "10/8/22", "My");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorHangHoaNoiSanXuat_Abnormal() {
        new HangHoaModel("1", "Laptop", 20000000, 10, "10/8/2022", "    ");
    }
    
    @Test
    public void testConstructorHangHoa_Normal() {
        HangHoaModel hangHoa = new HangHoaModel("1", "Laptop", 20000000, 10, "10/8/2022", "My");
        String expected = "1-Laptop-20000000.00-10-10/8/2022-My";
        String actual = hangHoa.toString();
        Assert.assertEquals(expected, actual);
    }
    
    @Test
    public void testDocGhiFile() {
        ArrayList<HangHoaModel> dsHangHoaExspected = HangHoaModel.getDsHangHoa();
        
        ArrayList<HangHoaModel> dsHangHoaActual = HangHoaModel.docFile();
    }
    
}
