package ontapjava1;

import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nhatt
 */
public class HangHoaModel implements Serializable {

    private static ArrayList<HangHoaModel> dsHangHoa;
    private static String LINK_FILE = "danhsach_hanghoa.txt";
    private String maHang;
    private String ten;
    private double gia;
    private int soLuong;
    private LocalDate ngaySanXuat;
    private String noiSanXuat;

    public static ArrayList<HangHoaModel> getDsHangHoa() {
        return dsHangHoa;
    }

    public HangHoaModel(String maHang, String ten, double gia, int soLuong, String ngaySanXuat, String noiSanXuat) {
        if (Uitilities.isEmptyOrWhiteSpace(maHang)) {
            throw new IllegalArgumentException("Mã hàng không hợp lệ");
        }

        if (getHangHoaByMaHang(maHang) != null) {
            throw new IllegalArgumentException("Mã hàng đã tồn tại");
        }

        if (Uitilities.isEmptyOrWhiteSpace(ten)) {
            throw new IllegalArgumentException("Tên không hợp lệ");
        }

        if (gia < 0) {
            throw new IllegalArgumentException("Giá không hợp lệ");
        }

        if (soLuong < 0) {
            throw new IllegalArgumentException("Số lượng không được âm");
        }

        if (Uitilities.isEmptyOrWhiteSpace(ngaySanXuat)) {
            throw new IllegalArgumentException("Ngày sản xuất không hợp lệ");
        }

        if (Uitilities.isEmptyOrWhiteSpace(noiSanXuat)) {
            throw new IllegalArgumentException("Nơi sản xuất không hợp lệ");
        }

        this.maHang = maHang;
        this.ten = ten;
        this.gia = gia;
        this.soLuong = soLuong;
        try {
            this.ngaySanXuat = LocalDate.parse(ngaySanXuat, DateTimeFormatter.ofPattern("d/M/yyyy"));
        } catch (Exception e) {
            throw new IllegalArgumentException("Định dạng ngày không hợp lệ");
        }
        this.noiSanXuat = noiSanXuat;

        if (dsHangHoa == null) {
            dsHangHoa = new ArrayList<>();
        }

        dsHangHoa.add(this);
    }

    public HangHoaModel() {
    }

    public String getMaHang() {
        return maHang;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        if (Uitilities.isEmptyOrWhiteSpace(maHang)) {
            throw new IllegalArgumentException("Mã hàng không hợp lệ");
        }
        this.ten = ten;
    }

    public double getGia() {
        return gia;
    }

    public void setGia(double gia) {
        if (gia < 0) {
            throw new IllegalArgumentException("Giá không hợp lệ");
        }
        this.gia = gia;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        if (soLuong < 0) {
            throw new IllegalArgumentException("Số lượng không được âm");
        }
        this.soLuong = soLuong;
    }

    public LocalDate getNgaySanXuat() {
        return ngaySanXuat;
    }

    public void setNgaySanXuat(String ngaySanXuat) {
        try {
            this.ngaySanXuat = LocalDate.parse(ngaySanXuat, DateTimeFormatter.ofPattern("d/M/yyyy"));
        } catch (Exception e) {
            throw new IllegalArgumentException("Dinh dang ngay khong hop le");
        }
    }

    public String getNoiSanXuat() {
        return noiSanXuat;
    }

    public void setNoiSanXuat(String noiSanXuat) {
        if (Uitilities.isEmptyOrWhiteSpace(noiSanXuat)) {
            throw new IllegalArgumentException("Nơi sản xuất không hợp lệ");
        }
        this.noiSanXuat = noiSanXuat;
    }

    public double getThanhTien() {
        return this.gia * this.soLuong;
    }

    public static HangHoaModel getHangHoaByMaHang(String maHang) {
        HangHoaModel hangHoaModel = null;
        if (HangHoaModel.getDsHangHoa() != null) {
            for (HangHoaModel hangHoa : HangHoaModel.getDsHangHoa()) {
                if (hangHoa.getMaHang().toLowerCase().equals(maHang.toLowerCase())) {
                    hangHoaModel = hangHoa;
                    break;
                }
            }
        }

        return hangHoaModel;
    }

    @Override
    public String toString() {
        return String.format("%s-%s-%.2f-%d-%s-%s", maHang, ten, gia, soLuong, ngaySanXuat.format(DateTimeFormatter.ofPattern("d/M/yyyy")), noiSanXuat);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.maHang);
        hash = 67 * hash + Objects.hashCode(this.ten);
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.gia) ^ (Double.doubleToLongBits(this.gia) >>> 32));
        hash = 67 * hash + this.soLuong;
        hash = 67 * hash + Objects.hashCode(this.ngaySanXuat);
        hash = 67 * hash + Objects.hashCode(this.noiSanXuat);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HangHoaModel other = (HangHoaModel) obj;
        if (Double.doubleToLongBits(this.gia) != Double.doubleToLongBits(other.gia)) {
            return false;
        }
        if (this.soLuong != other.soLuong) {
            return false;
        }
        if (!Objects.equals(this.maHang, other.maHang)) {
            return false;
        }
        if (!Objects.equals(this.ten, other.ten)) {
            return false;
        }
        if (!Objects.equals(this.noiSanXuat, other.noiSanXuat)) {
            return false;
        }
        if (!Objects.equals(this.ngaySanXuat, other.ngaySanXuat)) {
            return false;
        }
        return true;
    }

    public static boolean ghiFile() {
        try {
            FileOutputStream fos = new FileOutputStream(LINK_FILE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(dsHangHoa);

            fos.close();
            oos.close();

            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(HangHoaModel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HangHoaModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static ArrayList<HangHoaModel> docFile() {
        try {
            FileInputStream fis = new FileInputStream(LINK_FILE);
            ObjectInputStream ois = new ObjectInputStream(fis);

            dsHangHoa = (ArrayList<HangHoaModel>) ois.readObject();

            fis.close();
            ois.close();

            return dsHangHoa;
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } catch (ClassNotFoundException ex) {
        }

        return null;
    }
    
     public static HangHoaModel themHangHoa(String maHang, String ten, double gia, int soLuong, String ngaySanXuat, String noiSanXuat) {
        HangHoaModel hangHoaModel = null;

        hangHoaModel = new HangHoaModel(maHang, ten, gia, soLuong, ngaySanXuat, noiSanXuat);

        return hangHoaModel;
    }

    public static boolean suaHangHoa(HangHoaModel hangHoaModel, String ten, double gia, int soLuong, String ngaySanXuat, String noiSanXuat) {
        boolean status = true;
        hangHoaModel.setTen(ten);
        hangHoaModel.setGia(gia);
        hangHoaModel.setSoLuong(soLuong);
        hangHoaModel.setNgaySanXuat(ngaySanXuat);
        hangHoaModel.setNoiSanXuat(noiSanXuat);
        status = true;
        return status;
    }

    public static boolean xoaHangHoa(String maHang) {
        boolean status = false;
        HangHoaModel hangHoa = HangHoaModel.getHangHoaByMaHang(maHang);
        if (hangHoa != null) {
            HangHoaModel.getDsHangHoa().remove(hangHoa);
            status = true;
        }

        return status;
    }

    public static ArrayList<HangHoaModel> sapXepHangHoaTheoNoiSanXuat() {
        ArrayList<HangHoaModel> dsHangHoa = null;
        if (HangHoaModel.getDsHangHoa() != null && HangHoaModel.getDsHangHoa().size() > 0) {
            dsHangHoa = (ArrayList<HangHoaModel>) HangHoaModel.getDsHangHoa().clone();

        }

        if (dsHangHoa != null) {
            Collections.sort(dsHangHoa, new Comparator<HangHoaModel>() {
                @Override
                public int compare(HangHoaModel o1, HangHoaModel o2) {
                    int compare = 0;
                    if (o1.getNoiSanXuat().compareTo(o2.getNoiSanXuat()) > 0) {
                        compare = 1;
                    } else if (o1.getNoiSanXuat().compareTo(o2.getNoiSanXuat()) == 0) {
                        if (o1.getThanhTien() > o2.getThanhTien()) {
                            compare = -1;
                        } else if (o1.getThanhTien() < o2.getThanhTien()) {
                            compare = 1;
                        }
                    } else {
                        compare = -1;
                    }
                    return compare;
                }
            });
        }

        return dsHangHoa;
    }

    public static double tongThanhTien() {
        double sum = -1;
        if (HangHoaModel.getDsHangHoa() != null) {
            for (HangHoaModel hangHoaModel : HangHoaModel.getDsHangHoa()) {
                sum += hangHoaModel.getThanhTien();
            }
        }
        return sum;
    }
}
