/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontapjava1;

import java.util.regex.Pattern;

/**
 *
 * @author nhatt
 */
public class Uitilities {
        public static boolean isEmptyOrWhiteSpace(String str) {
        boolean check = false;
        Pattern pattern = Pattern.compile("^\\s*$");

        if (str == null) {
            check = true;
        } else {
            check = pattern.matcher(str).find();
        }
        return check;
    }
}
