/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontapjava1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javax.swing.JOptionPane;

/**
 *
 * @author nhatt
 */
public class HangHoaControl implements ActionListener {

    private HangHoaModel hangHoaModel;
    private HangHoaView hangHoaView;

    private static final String HOAN_TAT = "Hoàn tất";
    private static final String THEM = "Thêm";
    private static final String SUA = "Sửa";
    private static final String XOA = "Xoá";
    private static final String SAP_XEP = "Sắp xếp";
    private static final String HUY = "Huỷ";
    private static final String LUU = "Lưu";

    public HangHoaControl(HangHoaModel hangHoaModel, HangHoaView hangHoaView) {
        this.hangHoaModel = hangHoaModel;
        this.hangHoaView = hangHoaView;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();

        switch (action) {
            case THEM:
                hangHoaView.themHangHoa();
                break;
            case SUA:
                hangHoaView.layThongTinHangHoa();
                break;
            case HOAN_TAT:
                hangHoaView.suaHangHoa();
                break;
            case XOA:
                hangHoaView.xoaHangHoa();
                break;
            case SAP_XEP:
                hangHoaView.sapXepHangHoaTheoNoiSanXuat();
                break;
            case HUY:
                hangHoaView.huyHanhDong();
                break;
            case LUU:
                hangHoaView.luuFile();
                break;
        }
    }
}
