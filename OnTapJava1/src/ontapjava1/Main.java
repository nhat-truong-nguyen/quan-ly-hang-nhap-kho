/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontapjava1;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

/**
 *
 * @author nhatt
 */
public class Main {
        public static void main(String args[]) {
//        new HangHoaModel("001", "Điện thoại", 12200000, 10, "10/08/2022", "Trung Quốc");
//        new HangHoaModel("002", "Máy tính bảng", 15000000, 10, "10/08/2022", "Hàn Quốc");
//        new HangHoaModel("003", "Laptop", 22000000, 10, "10/08/2022", "Nhật Bản ");
//        new HangHoaModel("004", "Tai phone", 180000, 10, "10/08/2022", "Mỹ");
//        new HangHoaModel("005", "Đèn bàn học sinh", 120000, 10, "10/08/2022", "Việt Nam");
//        new HangHoaModel("006", "Giáo trình CNTT", 120000, 10, "10/08/2022", "Việt Nam");
//        new HangHoaModel("007", "Máy tính để bàn", 120000, 10, "10/08/2022", "Việt Nam");
//        new HangHoaModel("008", "Iphone 13", 2000000, 10, "10/08/2022", "Việt Nam");
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                HangHoaView hangHoaView = new HangHoaView();
                hangHoaView.setDefaultCloseOperation(EXIT_ON_CLOSE);
                hangHoaView.setResizable(false);
                hangHoaView.setVisible(true);
                hangHoaView.docDuLieu();
                hangHoaView.hienThiDSHangHoa(HangHoaModel.getDsHangHoa());
            }
        });
    }
}
