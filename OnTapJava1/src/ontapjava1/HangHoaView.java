/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontapjava1;

import static com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author nhatt
 */
dsfsdaffdsfdfsaf


















public class HangHoaView extends javax.swing.JFrame {

    private HangHoaModel hangHoaModel;
    private HangHoaControl hangHoaControl;

    public JButton getBtn_huy() {
        return btn_huy;
    }

    public JButton getBtn_luu() {
        return btn_luu;
    }

    public JButton getBtn_sapxep() {
        return btn_sapxep;
    }

    public JButton getBtn_sua() {
        return btn_sua;
    }

    public JButton getBtn_them() {
        return btn_them;
    }

    public JButton getBtn_xoa() {
        return btn_xoa;
    }

    public JLabel getErr_gia() {
        return err_gia;
    }

    public JLabel getErr_mahang() {
        return err_mahang;
    }

    public JLabel getErr_ngaysanxuat() {
        return err_ngaysanxuat;
    }

    public JLabel getErr_noisanxuat() {
        return err_noisanxuat;
    }

    public JLabel getErr_soluong() {
        return err_soluong;
    }

    public JTable getTbl_hanghoa() {
        return tbl_hanghoa;
    }

    public JTextField getTf_gia() {
        return tf_gia;
    }

    public JTextField getTf_mahang() {
        return tf_mahang;
    }

    public JTextField getTf_ngaysanxuat() {
        return tf_ngaysanxuat;
    }

    public JTextField getTf_noisanxuat() {
        return tf_noisanxuat;
    }

    public JTextField getTf_soluong() {
        return tf_soluong;
    }

    public JTextField getTf_tenhang() {
        return tf_tenhang;
    }

    public HangHoaView() {
        initComponents();

        addAction();
        this.setLocationRelativeTo(null);
        this.setTitle("Nguyễn Nhật Trường - 21211TT0042");
    }

    public void addAction() {
        HangHoaControl hangHoaControl = new HangHoaControl(hangHoaModel, this);
        this.hangHoaControl = hangHoaControl;
        btn_them.addActionListener(hangHoaControl);
        btn_sua.addActionListener(hangHoaControl);
        btn_xoa.addActionListener(hangHoaControl);
        btn_sapxep.addActionListener(hangHoaControl);
        btn_huy.addActionListener(hangHoaControl);
        btn_luu.addActionListener(hangHoaControl);
    }

    public HashMap<String, Object> layDuLieuTuForm() {
        HashMap<String, Object> data = new HashMap<String, Object>();

        data.put("maHang", tf_mahang.getText());
        data.put("ten", tf_tenhang.getText());
        try {
            data.put("gia", Double.parseDouble(tf_gia.getText()));
        } catch (Exception e) {
            data.put("gia", -1);
        }
        try {
            data.put("soLuong", Integer.parseInt(tf_soluong.getText()));
        } catch (Exception e) {
            data.put("soLuong", -1);
        }

        data.put("ngaySanXuat", tf_ngaysanxuat.getText());
        data.put("noiSanXuat", tf_noisanxuat.getText());

        return data;
    }

    public void xoaDuLieuTrongForm() {
        tf_mahang.setText("");
        tf_tenhang.setText("");
        tf_gia.setText("");
        tf_soluong.setText("");
        tf_ngaysanxuat.setText("");
        tf_noisanxuat.setText("");
    }

    public void hienThiDSHangHoa(ArrayList<HangHoaModel> dsHangHoa) {
        if (dsHangHoa == null || dsHangHoa.size() == 0) {
            return;
        }
        String[] header = new String[]{
            "Mã Hàng", "Tên hàng", "Giá", "Số lượng", "Ngày sản xuất", "Nơi sản xuất", "Thành tiền (VND)"
        };

        String[] data = new String[7];
        DefaultTableModel model = new DefaultTableModel(header, 0);
        this.tbl_hanghoa.setModel(model);

        for (HangHoaModel hangHoaModel : dsHangHoa) {

            data[0] = hangHoaModel.getMaHang();
            data[1] = hangHoaModel.getTen();
            data[2] = String.format("%,.0f", hangHoaModel.getGia());
            data[3] = hangHoaModel.getSoLuong() + "";
            data[4] = hangHoaModel.getNgaySanXuat().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            data[5] = hangHoaModel.getNoiSanXuat();
            data[6] = String.format("%,.0f", hangHoaModel.getGia());
            model.addRow(data);
        }

        hienThiTongThanhTien();
    }

    public void hienThiTongThanhTien() {
        double sum = HangHoaModel.tongThanhTien();

        if (sum > 0) {
            lbl_tongthanhtien.setText(String.format("%,.0f VND", sum));
        }
    }

    public void themHangHoa() {
        HashMap<String, Object> data = layDuLieuTuForm();
        HangHoaModel hangHoa = null;

        try {
            hangHoa = HangHoaModel.themHangHoa((String) data.get("maHang"), (String) data.get("ten"), (double) data.get("gia"), (int) data.get("soLuong"), (String) data.get("ngaySanXuat"), (String) data.get("noiSanXuat"));

            hienThiDSHangHoa(HangHoaModel.getDsHangHoa());
        } catch (Exception e) {
            showMessage(e.getMessage());
            hangHoa = null;
        }

        if (hangHoa != null) {
            showMessage("Thêm hàng hoá thành công");
            luuFile();
        } else {
            showMessage("Thêm hàng hoá thất bại");
        }
    }

    public void layThongTinHangHoa() {
        int rowIndex = tbl_hanghoa.getSelectedRow();
        HangHoaModel hangHoaModel = null;
        if (rowIndex > -1) {
            hangHoaModel = HangHoaModel.getDsHangHoa().get(rowIndex);

            tf_mahang.setText(hangHoaModel.getMaHang());
            tf_tenhang.setText(hangHoaModel.getTen());
            tf_gia.setText(String.format("%.0f", hangHoaModel.getGia()));
            tf_soluong.setText(hangHoaModel.getSoLuong() + "");
            tf_ngaysanxuat.setText(hangHoaModel.getNgaySanXuat().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
            tf_noisanxuat.setText(hangHoaModel.getNoiSanXuat());

            tf_mahang.setEditable(false);
            btn_sua.setText("Hoàn tất");
        } else {
            showMessage("Bạn chưa chọn hàng hoá");
        }
    }

    public void suaHangHoa() {
        HashMap<String, Object> data = layDuLieuTuForm();
        HangHoaModel hangHoaModel = HangHoaModel.getHangHoaByMaHang((String) data.get("maHang"));
        boolean check = false;

        try {
            check = HangHoaModel.suaHangHoa(hangHoaModel, (String) data.get("ten"), (double) data.get("gia"), (int) data.get("soLuong"), (String) data.get("ngaySanXuat"), (String) data.get("noiSanXuat"));
            check = true;
            hienThiDSHangHoa(HangHoaModel.getDsHangHoa());
        } catch (Exception e) {
            showMessage(e.getMessage());
            check = false;
        }

        if (check) {
            showMessage("Sửa thành công");
            luuFile();
            huyHanhDong();
        } else {
            showMessage("Sửa thất bại");
        }

        huyHanhDong();
    }

    public void xoaHangHoa() {
        boolean check = false;
        int rowIndex = tbl_hanghoa.getSelectedRow();
        String maHang = "";
        if (rowIndex > -1) {
            maHang = HangHoaModel.getDsHangHoa().get(rowIndex).getMaHang();
            check = HangHoaModel.xoaHangHoa(maHang);
            hienThiDSHangHoa(HangHoaModel.getDsHangHoa());

            if (check) {
                showMessage("Xoá thành công");
                luuFile();
            } else {
                showMessage("Xoá thất bại");
            }
        } else {
            showMessage("Bạn chưa chọn hàng hoá");
        }
    }

    public void sapXepHangHoaTheoNoiSanXuat() {
        ArrayList<HangHoaModel> dsHangHoa = HangHoaModel.sapXepHangHoaTheoNoiSanXuat();

        if (dsHangHoa != null) {
            hienThiDSHangHoa(dsHangHoa);
        } else {
            showMessage("Danh sách hàng hoá rỗng");
        }
    }

    public void huyHanhDong() {
        if (!tf_mahang.isEditable()) {
            tf_mahang.setEditable(true);
        }

        if (btn_sua.getText() != "Sửa") {
            btn_sua.setText("Sửa");
        }
        xoaDuLieuTrongForm();
        hienThiDSHangHoa(HangHoaModel.getDsHangHoa());
    }

    public void luuFile() {
        boolean status = HangHoaModel.ghiFile();

        if (status) {
            showMessage("Lưu thành công");
        } else {
            showMessage("Lưu thất bại");
        }
    }

    public void docDuLieu() {
        if (HangHoaModel.docFile() != null) {
            showMessage("Đọc file thành công");
        } else {
            showMessage("Đọc file thất bại");
        }
    }

    public void showMessage(String message) {
        JOptionPane.showMessageDialog(this, message);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btn_them = new javax.swing.JButton();
        btn_sua = new javax.swing.JButton();
        btn_xoa = new javax.swing.JButton();
        btn_sapxep = new javax.swing.JButton();
        btn_huy = new javax.swing.JButton();
        btn_luu = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_hanghoa = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        lbl_tongthanhtien = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        tf_mahang = new javax.swing.JTextField();
        tf_tenhang = new javax.swing.JTextField();
        tf_soluong = new javax.swing.JTextField();
        tf_gia = new javax.swing.JTextField();
        tf_noisanxuat = new javax.swing.JTextField();
        tf_ngaysanxuat = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        err_mahang = new javax.swing.JLabel();
        err_gia = new javax.swing.JLabel();
        err_ngaysanxuat = new javax.swing.JLabel();
        err_tenhang = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        err_noisanxuat = new javax.swing.JLabel();
        err_soluong = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btn_them.setText("Thêm");
        jPanel1.add(btn_them);

        btn_sua.setText("Sửa");
        jPanel1.add(btn_sua);

        btn_xoa.setText("Xoá");
        jPanel1.add(btn_xoa);

        btn_sapxep.setText("Sắp xếp");
        jPanel1.add(btn_sapxep);

        btn_huy.setText("Huỷ");
        jPanel1.add(btn_huy);

        btn_luu.setText("Lưu");
        jPanel1.add(btn_luu);

        String[] header =  new String [] {
            "Mã Hàng", "Tên hàng", "Giá", "Số lượng", "Ngày sản xuất", "Nơi sản xuất", "Thành tiền (VND)"
        }
        tbl_hanghoa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
            },
            header
        ));
        tbl_hanghoa.setRowHeight(25);
        jScrollPane1.setViewportView(tbl_hanghoa);

        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Tổng thành tiền:");

        lbl_tongthanhtien.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbl_tongthanhtien, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(468, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbl_tongthanhtien, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Thông tin hàng hoá"));

        jLabel1.setText("Giá");

        jLabel2.setText("Mã hàng");

        jLabel3.setText("Ngày sản xuất");

        jLabel4.setText("Tên hàng");

        jLabel5.setText("Số lượng");

        jLabel6.setText("Nơi sản xuất");

        err_mahang.setText(" ");

        err_gia.setText(" ");

        err_ngaysanxuat.setText(" ");

        err_tenhang.setText(" ");

        jLabel13.setText(" ");

        jLabel12.setText(" ");

        jLabel14.setText(" ");

        err_noisanxuat.setText(" ");

        err_soluong.setText(" ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel7)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_mahang, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_gia, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_ngaysanxuat, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(err_mahang, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(err_gia, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(err_ngaysanxuat, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addGap(24, 24, 24)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_tenhang, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_soluong, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_noisanxuat, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(err_noisanxuat, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(err_soluong, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(err_tenhang, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(657, 657, 657)
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(657, 657, 657)
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {tf_gia, tf_mahang, tf_ngaysanxuat});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {tf_noisanxuat, tf_soluong, tf_tenhang});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel7)
                .addGap(12, 12, 12)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_mahang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_tenhang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(err_mahang)
                            .addComponent(jLabel4)
                            .addComponent(err_tenhang)
                            .addComponent(jLabel14)
                            .addComponent(jLabel12))))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_gia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tf_soluong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(err_soluong))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(err_gia)
                            .addComponent(jLabel5))))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_ngaysanxuat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_noisanxuat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(err_ngaysanxuat)
                    .addComponent(jLabel6)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(err_noisanxuat)))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 839, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jPanel2, jPanel6});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel4.setLayout(new java.awt.BorderLayout());

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 0, 255));
        jLabel8.setText("CHƯƠNG TRÌNH QUẢN LÝ HÀNG NHẬP KHO");
        jPanel5.add(jLabel8);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(845, 845, 845))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 828, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_huy;
    private javax.swing.JButton btn_luu;
    private javax.swing.JButton btn_sapxep;
    private javax.swing.JButton btn_sua;
    private javax.swing.JButton btn_them;
    private javax.swing.JButton btn_xoa;
    private javax.swing.JLabel err_gia;
    private javax.swing.JLabel err_mahang;
    private javax.swing.JLabel err_ngaysanxuat;
    private javax.swing.JLabel err_noisanxuat;
    private javax.swing.JLabel err_soluong;
    private javax.swing.JLabel err_tenhang;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbl_tongthanhtien;
    private javax.swing.JTable tbl_hanghoa;
    private javax.swing.JTextField tf_gia;
    private javax.swing.JTextField tf_mahang;
    private javax.swing.JTextField tf_ngaysanxuat;
    private javax.swing.JTextField tf_noisanxuat;
    private javax.swing.JTextField tf_soluong;
    private javax.swing.JTextField tf_tenhang;
    // End of variables declaration//GEN-END:variables
}
